package com.syject.mavrik.utlis

import com.syject.mavrik.MavrikApplication

fun Int.asString() = MavrikApplication.instance.getString(this)