package com.syject.mavrik.utlis

import androidx.core.content.ContextCompat
import com.syject.mavrik.MavrikApplication

fun Int.asColor() = ContextCompat.getColor(MavrikApplication.instance, this)