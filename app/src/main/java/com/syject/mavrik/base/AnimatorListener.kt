package com.syject.mavrik.base

import android.animation.Animator

interface AnimatorListener : Animator.AnimatorListener {
    override fun onAnimationRepeat(animator: Animator) {
    }

    override fun onAnimationEnd(animator: Animator) {
    }

    override fun onAnimationCancel(animator: Animator) {
    }

    override fun onAnimationStart(animator: Animator) {
    }
}