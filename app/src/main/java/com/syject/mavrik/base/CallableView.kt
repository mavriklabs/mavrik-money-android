package com.syject.mavrik.base

interface CallableView {
    abstract fun call(code: Int)
}