package com.syject.mavrik.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment

abstract class BaseDialog<
        Binding : ViewDataBinding,
        ViewModel : BaseViewModel
> : DialogFragment(), CallableView {

    protected lateinit var binding: Binding
    lateinit var viewModel: ViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupBinding()
        initUI()
    }

    protected abstract fun getLayoutId(): Int

    private fun setupBinding() {
        viewModel = provideViewModel()
        binding.setVariable(getViewModelVariableId(), viewModel)
        initBindingVariables()
    }

    protected abstract fun provideViewModel(): ViewModel
    protected abstract fun getViewModelVariableId(): Int // returns BR.viewModel
    protected open fun initBindingVariables() {}

    private fun initUI() {
        initViews()
        initListeners()
    }

    protected open fun initViews() {}
    protected open fun initListeners() {}

}