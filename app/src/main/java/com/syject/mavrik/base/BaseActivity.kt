package com.syject.mavrik.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseActivity<
        Binding : ViewDataBinding,
        ViewModel : BaseViewModel
> : AppCompatActivity(), CallableView {

    protected lateinit var binding: Binding
    lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBinding()
        initUI()
    }

    private fun initBinding() {
        binding = DataBindingUtil.setContentView(this, getLayoutId())
        binding.lifecycleOwner = this
        setupBinding()
    }

    private fun setupBinding() {
        viewModel = provideViewModel()
        binding.setVariable(getViewModelVariableId(), viewModel)
        initBindingVariables()
    }

    protected abstract fun getLayoutId(): Int
    protected abstract fun provideViewModel(): ViewModel
    protected abstract fun getViewModelVariableId(): Int // returns BR.viewModel
    protected open fun initBindingVariables() {}

    private fun initUI() {
        initViews()
        initListeners()
    }

    protected open fun initViews() {}
    protected open fun initListeners() {}

    override fun onBackPressed() = finish()

    fun navigateTo(cls: Class<*>) {
        startActivity(Intent(this, cls))
    }

}