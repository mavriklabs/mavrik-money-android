package com.syject.mavrik.base

import androidx.databinding.BaseObservable
import io.reactivex.Observable

abstract class BaseViewModel(
    private val view: CallableView
) : BaseObservable() {

    protected fun callView(code: Int) {
        view.call(code)
    }

}