package com.syject.mavrik.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<Item, Holder : RecyclerView.ViewHolder>(
        protected val onItemClick: ((item: Item, position: Int) -> Unit)? = null
) : RecyclerView.Adapter<Holder>() {

    protected val items = ArrayList<Item>()

    override fun getItemCount(): Int {
        return items.size
    }

    fun setData(newData: List<Item>) {
        clear()
        items.addAll(newData)
        notifyDataSetChanged()
    }

    fun removeItem(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    fun getDataList(): List<Item> {
        return items
    }

    fun addItem(item: Item) {
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    fun clear() {
        items.clear()
    }

    fun updateItem(position: Int) {
        notifyItemChanged(position, Unit)
    }
}