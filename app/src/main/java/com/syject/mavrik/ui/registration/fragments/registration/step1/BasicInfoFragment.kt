package com.syject.mavrik.ui.registration.fragments.registration.step1

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep1BasicInfoBinding

class BasicInfoFragment : BaseFragment<FragmentStep1BasicInfoBinding, BasicInfoViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_step_1_basic_info

    override fun provideViewModel(): BasicInfoViewModel = BasicInfoViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}