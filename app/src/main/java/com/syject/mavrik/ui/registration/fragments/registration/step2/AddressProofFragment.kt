package com.syject.mavrik.ui.registration.fragments.registration.step2

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep2AddressProofBinding
import com.syject.mavrik.utlis.Constants

class AddressProofFragment
    : BaseFragment<FragmentStep2AddressProofBinding, AddressProofViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_step_2_address_proof

    override fun provideViewModel(): AddressProofViewModel = AddressProofViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_MAKE_PHOTOS -> {
                (requireParentFragment() as Step2ParentFragment).makePhotos()
            }
        }
    }
}