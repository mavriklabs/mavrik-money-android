package com.syject.mavrik.ui.registration.fragments.registration.step3

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep3SelectBinding
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants

class SelectFragment : BaseFragment<FragmentStep3SelectBinding, SelectViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_step_3_select

    override fun provideViewModel(): SelectViewModel = SelectViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SELECT_PIN -> {
                (requireParentFragment() as Step3ParentFragment).setSetupPinFragment()
            }
            Constants.CALL_CODE_SELECT_TOUCH_ID -> {
                (requireActivity() as RegistrationActivity).onRegistrationCompleted()
            }
        }
    }
}