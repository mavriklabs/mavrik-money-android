package com.syject.mavrik.ui.splash

import android.animation.Animator
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.AnimatorListener
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.databinding.ActivitySplashBinding
import com.syject.mavrik.ui.login.LoginActivity
import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>() {

    override fun initViews() {
        GlobalScope.launch {
            delay(1000)
            playAnimation()
        }
    }

    private fun playAnimation() {
        logo.animate().scaleY(0f).setDuration(2000)
        logo.animate().scaleX(0f).setDuration(2000)
            .setListener(object : AnimatorListener {
                override fun onAnimationEnd(animator: Animator) {
                    navigateTo(LoginActivity::class.java)
                }
            })
        logo.animate().alpha(0f).setDuration(400)
    }

    override fun getLayoutId(): Int = R.layout.activity_splash

    override fun provideViewModel() : SplashViewModel = SplashViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}