package com.syject.mavrik.ui.make.photo.fragments

import android.graphics.Bitmap
import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class MakePhotoFragmentViewModel(view: CallableView): BaseViewModel(view) {

    var picture: Bitmap? = null

    fun onMakePhotoButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAKE_PHOTO_BUTTON_CLICK)
    }

}