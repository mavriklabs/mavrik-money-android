package com.syject.mavrik.ui.registration.fragments.registration.step3

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class SelectViewModel(view: CallableView) : BaseViewModel(view) {

    fun firstOptionClick(view: View) {
        callView(Constants.CALL_CODE_SELECT_PIN)
    }

    fun secondOptionClick(view: View) {
        callView(Constants.CALL_CODE_SELECT_TOUCH_ID)
    }

}