package com.syject.mavrik.ui.main.fragments.account.balance

import androidx.core.content.res.ResourcesCompat
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentMainAccountBalanceBinding
import com.syject.mavrik.model.account.Account
import com.syject.mavrik.ui.main.MainActivity
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_main_account_balance.*

class AccountBalanceFragment(
    val account: Account
) : BaseFragment<FragmentMainAccountBalanceBinding, AccountBalanceViewModel>() {

    override fun initViews() {
        seekBar.customTickTextsTypeface(
            ResourcesCompat.getFont(requireContext(), R.font.montserrat)!!
        )
    }

    override fun getLayoutId(): Int = R.layout.fragment_main_account_balance

    override fun provideViewModel(): AccountBalanceViewModel = AccountBalanceViewModel(this)

    override fun initBindingVariables() {
        binding.account = account
    }

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_OPEN_MENU -> {
                (requireActivity() as MainActivity).openMenu()
            }
            Constants.CALL_CODE_MAIN_BALANCE -> {
                (requireActivity() as MainActivity).setBalanceFragment()
            }
        }
    }
}