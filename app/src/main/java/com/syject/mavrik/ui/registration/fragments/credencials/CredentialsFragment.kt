package com.syject.mavrik.ui.registration.fragments.credencials

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentCrdentialsBinding
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants

class CredentialsFragment : BaseFragment<FragmentCrdentialsBinding, CredentialsViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_crdentials

    override fun provideViewModel(): CredentialsViewModel = CredentialsViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_CREDENTIALS_BACK -> {
                (requireActivity() as RegistrationActivity).onBackPressed()
            }
            Constants.CALL_CODE_CREDENTIALS_NEXT -> {
                (requireActivity() as RegistrationActivity).setExplainerFragment()
            }
        }
    }
}