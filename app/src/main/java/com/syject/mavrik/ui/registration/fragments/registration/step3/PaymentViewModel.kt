package com.syject.mavrik.ui.registration.fragments.registration.step3

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class PaymentViewModel(view: CallableView) : BaseViewModel(view) {

    var card = ""
    var cardholderName = ""
    var expiryDate = ""
    var cvv = ""

    fun onNextButtonClick(view: View) {
        callView(Constants.CALL_CODE_REGISTER_NEXT_BUTTON_CLICK)
    }

}