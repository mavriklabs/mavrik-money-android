package com.syject.mavrik.ui.dialogs.authenticating

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogAuthenticatingBinding

class AuthenticatingDialog : BaseDialog<DialogAuthenticatingBinding, AuthenticatingViewModel>() {
    override fun getLayoutId(): Int = R.layout.dialog_authenticating

    override fun provideViewModel(): AuthenticatingViewModel = AuthenticatingViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}