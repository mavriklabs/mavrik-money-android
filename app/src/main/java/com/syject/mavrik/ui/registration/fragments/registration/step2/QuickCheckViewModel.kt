package com.syject.mavrik.ui.registration.fragments.registration.step2

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class QuickCheckViewModel(view: CallableView) : BaseViewModel(view) {

    fun onRedoButtonClick(view: View) {
        callView(Constants.CALL_CODE_QUICK_CHECK_REDO)
    }

    fun onNextButtonClick(view: View) {
        callView(Constants.CALL_CODE_QUICK_CHECK_NEXT)
    }

}