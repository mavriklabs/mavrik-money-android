package com.syject.mavrik.ui.main.fragments.balance

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentMainBalanceBinding
import com.syject.mavrik.model.account.Account
import com.syject.mavrik.ui.main.MainActivity
import com.syject.mavrik.utlis.Constants

class BalanceFragment(
    private val account: Account
) : BaseFragment<FragmentMainBalanceBinding, BalanceViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_main_balance

    override fun provideViewModel(): BalanceViewModel = BalanceViewModel(this)

    override fun initBindingVariables() {
        binding.account = account
    }

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_OPEN_MENU -> {
                (requireActivity() as MainActivity).openMenu()
            }
            Constants.CALL_CODE_MAIN_BALANCE_BACK -> {
                (requireActivity() as MainActivity).backFromBalanceFragment()
            }
        }
    }
}