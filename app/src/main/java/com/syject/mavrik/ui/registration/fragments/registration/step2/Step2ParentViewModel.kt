package com.syject.mavrik.ui.registration.fragments.registration.step2

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class Step2ParentViewModel(view: CallableView) : BaseViewModel(view) {

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_REGISTER_BACK)
    }

}