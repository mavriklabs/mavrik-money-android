package com.syject.mavrik.ui.make.photo

import android.app.Activity
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.databinding.ActivityMakePhotoBinding
import com.syject.mavrik.ui.make.photo.fragments.MakePhotoFragment
import com.syject.mavrik.ui.registration.fragments.registration.step2.QuickCheckFragment
import com.syject.mavrik.utlis.Constants

class MakePhotoActivity : BaseActivity<ActivityMakePhotoBinding, MakePhotoViewModel>() {

    private val title1 = "Passport"
    private val instruction1 = "Place your original passport within the frame and\ntake a picture. (No scans or photocopies)"
    private val title2 = "Address Proof Front"
    private val instruction2 = "Place your original document within the frame and\ntake a picture. (No scans or photocopies)"
    private val title3 = "Address Proof Back"
    private val instruction3 = "Place your original document within the frame and\ntake a picture. (No scans or photocopies)"

    private val childFragments = listOf(
        MakePhotoFragment(title1, instruction1, 0) { setCurrentFragment(1) },
        QuickCheckFragment(0, { setCurrentFragment(0) }, {
            setCurrentFragment(2)
        }),
        MakePhotoFragment(title2, instruction2, 1) { setCurrentFragment(3) },
        QuickCheckFragment(1, { setCurrentFragment(2) }, {
            setCurrentFragment(4)
        }),
        MakePhotoFragment(title3, instruction3, 2) { setCurrentFragment(5) },
        QuickCheckFragment(2, { setCurrentFragment(4) }, {
            onLastPhotoTaken()
        })
    )

    override fun initViews() {
        setCurrentFragment(0)
    }

    private fun setCurrentFragment(position: Int) {
        val currentFragment = childFragments[position]
        if (currentFragment is MakePhotoFragment) {
            setMakePhotoFragment(currentFragment, position)
        } else if (currentFragment is QuickCheckFragment) {
            setQuickCheckFragment(currentFragment, position)
        }
    }

    private fun setMakePhotoFragment(fragment: MakePhotoFragment, position: Int) {
        if (fragment.isHidden) {
            supportFragmentManager
                .beginTransaction()
                .hide(childFragments[position + 1])
                .show(fragment)
                .commit()
        } else {
            when (position) {
                2 -> closeCamera(0)
                4 -> closeCamera(2)
            }
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
        }
    }

    private fun closeCamera(position: Int) {
        val camera = (childFragments[position] as MakePhotoFragment).binding.camera
        if (camera.isOpened) camera.close()
    }

    private fun setQuickCheckFragment(fragment: QuickCheckFragment, position: Int) {
        if (fragment.isHidden) {
            supportFragmentManager
                .beginTransaction()
                .hide(childFragments[position - 1])
                .show(fragment)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .hide(childFragments[position - 1])
                .add(R.id.fragmentContainer, fragment)
                .commit()
        }
    }

    private fun onLastPhotoTaken() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun getLayoutId(): Int = R.layout.activity_make_photo

    override fun provideViewModel(): MakePhotoViewModel = MakePhotoViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_MAKE_PHOTOS_BACK -> {
                onBackPressed()
            }
        }
    }
}