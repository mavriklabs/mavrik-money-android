package com.syject.mavrik.ui.registration.fragments.registration.step1

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.model.enums.country.Country
import com.syject.mavrik.utlis.Constants

class AccountDetailsViewModel(view: CallableView) : BaseViewModel(view) {

    var phone = ""

    var country = Country.INDIA
    set(value) {
        field = value
        notifyChange()
    }

    fun onSelectCountryClickListener(view: View) {
        callView(Constants.CALL_CODE_SHOW_SELECT_COUNTRY_DIALOG)
    }

}