package com.syject.mavrik.ui.dialogs.verification.verified.fragments

import androidx.fragment.app.DialogFragment
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.DialogVerificationVerifiedFragmentSuccessfulBinding
import com.syject.mavrik.utlis.Constants

class SuccessfulFragment :
    BaseFragment<DialogVerificationVerifiedFragmentSuccessfulBinding, SuccessfulViewModel>() {
    override fun getLayoutId(): Int = R.layout.dialog_verification_verified_fragment_successful

    override fun provideViewModel(): SuccessfulViewModel = SuccessfulViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_DISMISS_DIALOG -> {
                (requireParentFragment() as DialogFragment).dismiss()
            }
        }
    }
}