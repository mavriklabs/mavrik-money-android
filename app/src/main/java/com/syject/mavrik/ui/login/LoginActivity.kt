package com.syject.mavrik.ui.login

import android.animation.Animator
import android.content.Intent
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.AnimatorListener
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.databinding.ActivityLoginBinding
import com.syject.mavrik.ui.login.fragments.email.EmailLoginFragment
import com.syject.mavrik.ui.login.fragments.pin.PinLoginFragment
import com.syject.mavrik.ui.main.MainActivity
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>() {

    override fun initViews() {
        setPinFragment()
        playAnimation()
    }

    fun setPinFragment() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainer, PinLoginFragment())
            addToBackStack(null)
            commit()
        }
    }

    fun setEmailFragment() {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentContainer, EmailLoginFragment())
            addToBackStack(null)
            commit()
        }
    }

    private fun playAnimation() {
        GlobalScope.launch {
            delay(500)
            logo.animate().translationY(100f).setDuration(250)
                .setListener(object : AnimatorListener {
                    override fun onAnimationEnd(animator: Animator) {
                        logo.animate().translationY(0f).setDuration(250)
                    }
                })
        }
    }

    fun onLoginCompleted() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun provideViewModel(): LoginViewModel = LoginViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}