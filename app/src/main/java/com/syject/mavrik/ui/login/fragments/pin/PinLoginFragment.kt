package com.syject.mavrik.ui.login.fragments.pin

import android.content.pm.PackageManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.core.text.toSpannable
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentLoginPinBinding
import com.syject.mavrik.ui.login.LoginActivity
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_login_pin.*

class PinLoginFragment : BaseFragment<FragmentLoginPinBinding, PinLoginViewModel>() {

    private val linkText = arrayOf("Or login with ", "Email", " or ", "Touch ID", " .")

    override fun initViews() {
        setSpannableText()
        keyboard.inputConnection = pinField.onCreateInputConnection(EditorInfo())
        keyboard.onDoneButtonClick = viewModel::onDoneButtonClick
    }

    private fun setSpannableText() {
        var ssb = SpannableStringBuilder()
            .append(linkText[0])
            .append(linkText[1], getEmailClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        if (requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            ssb = ssb
                .append(linkText[2])
                .append(linkText[3], getTouchClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        ssb = ssb
            .append(linkText[4])
        navigationLinks.text = ssb.toSpannable()
        navigationLinks.movementMethod = LinkMovementMethod.getInstance()
    }

    override fun initListeners() {
        pinField.setOnClickListener {
            keyboard.visibility = View.VISIBLE
        }
    }

    private fun getEmailClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {
            (requireActivity() as LoginActivity).setEmailFragment()
        }
    }

    private fun getTouchClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {

        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_login_pin

    override fun provideViewModel(): PinLoginViewModel = PinLoginViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SIGN_UP -> {
                navigateTo(RegistrationActivity::class.java)
            }
            Constants.CALL_CODE_LOGIN_COMPLETED -> {
                (requireActivity() as LoginActivity).onLoginCompleted()
            }
        }
    }
}