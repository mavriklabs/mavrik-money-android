package com.syject.mavrik.ui.custom.recycler.country

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.syject.mavrik.databinding.CountryItemBinding

class CountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val binding: CountryItemBinding? = DataBindingUtil.bind(itemView)

}