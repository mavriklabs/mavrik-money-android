package com.syject.mavrik.ui.registration.fragments.explainer

import android.view.View
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants
import com.syject.mavrik.utlis.asString

class ExplainerParentViewModel(view: CallableView) : BaseViewModel(view) {

    var buttonText = R.string.get_started.asString()

    fun onButtonClick(view: View) {
        callView(Constants.CALL_CODE_EXPLAINER_FRAGMENT_BUTTON_CLICK)
    }

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_EXPLAINER_BACK)
    }

}