package com.syject.mavrik.ui.custom.recycler.notification

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.syject.mavrik.databinding.NotificationItemBinding

class NotificationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val binding: NotificationItemBinding? = DataBindingUtil.bind(itemView)

}