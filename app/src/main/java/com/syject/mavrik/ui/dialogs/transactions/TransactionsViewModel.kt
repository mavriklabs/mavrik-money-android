package com.syject.mavrik.ui.dialogs.transactions

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.model.transaction.Transaction
import com.syject.mavrik.ui.custom.recycler.transaction.TransactionRecyclerViewAdapter
import com.syject.mavrik.utlis.Constants

class TransactionsViewModel(view: CallableView) : BaseViewModel(view) {

    val transactionAdapter: TransactionRecyclerViewAdapter =
        TransactionRecyclerViewAdapter().apply {
            setData(
                listOf(
                    Transaction(
                        "2019",
                        "Withdrawn to Bank",
                        "- $ 1,500.00",
                        "xxxx-xxxx-6789",
                        "22 Jan 2019, 11:11 am"
                    ),
                    Transaction(
                        "2018",
                        "Deposited to Crypto Wallet",
                        "+ 70 ETH",
                        "0xC8B6ec70d3798457CH...CDE1",
                        "19 Dec 2018, 10:40 pm"
                    ),
                    Transaction(
                        "2018",
                        "Withdrawn to Crypto Wallet",
                        "- 20 FIA",
                        "kjshdfu4834578sdfug475...ABCD",
                        "1 Dec 2018, 10:40 pm"
                    )
                )
            )
        }

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_DISMISS_DIALOG)
    }

}