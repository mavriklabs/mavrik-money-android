package com.syject.mavrik.ui.registration.fragments.explainer

import androidx.viewpager.widget.ViewPager
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentExplainerParentBinding
import com.syject.mavrik.ui.custom.ViewPagerAdapter
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants
import com.syject.mavrik.utlis.asString
import kotlinx.android.synthetic.main.fragment_explainer_parent.*

class ExplainerParentFragment
    : BaseFragment<FragmentExplainerParentBinding, ExplainerParentViewModel>(),
    ViewPager.OnPageChangeListener {

    private val firstPagePoints = arrayOf(
        "Basic information collection",
        "Document submission",
        R.string.for_text.asString()
    )
    private val secondPagePoints = arrayOf(
        "Passport",
        "Address Proof"
    )

    override fun initViews() {
        pager.adapter = ViewPagerAdapter(
            childFragmentManager,
            listOf(
                ExplainerChildFragment(R.string.three_steps.asString(), firstPagePoints),
                ExplainerChildFragment(R.string.needed_documents.asString(), secondPagePoints)
            )
        )
        dotsIndicator.setupWithViewPager(pager)
    }

    override fun initListeners() {
        pager.addOnPageChangeListener(this)
    }

    override fun onPageScrollStateChanged(state: Int) {}
    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}
    override fun onPageSelected(position: Int) {
        viewModel.buttonText = if (position == 0) {
            R.string.get_started.asString()
        } else {
            R.string.got_it_exclamation.asString()
        }
        viewModel.notifyChange()
    }

    override fun getLayoutId(): Int = R.layout.fragment_explainer_parent

    override fun provideViewModel(): ExplainerParentViewModel = ExplainerParentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_EXPLAINER_FRAGMENT_BUTTON_CLICK -> {
                if (pager.currentItem == 0) {
                    pager.currentItem = 1
                } else {
                    (requireActivity() as RegistrationActivity).setStep1Fragment()
                }
            }
            Constants.CALL_CODE_EXPLAINER_BACK -> {
                if (pager.currentItem == 1) {
                    pager.currentItem = 0
                } else {
                    (requireActivity() as RegistrationActivity).setCredentialFragment()
                }
            }
        }
    }
}