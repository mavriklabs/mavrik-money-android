package com.syject.mavrik.ui.custom.recycler.transaction

import android.view.LayoutInflater
import android.view.ViewGroup
import com.syject.mavrik.base.BaseRecyclerViewAdapter
import com.syject.mavrik.databinding.TransactionItemBinding
import com.syject.mavrik.model.transaction.Transaction

class TransactionRecyclerViewAdapter :
    BaseRecyclerViewAdapter<Transaction, TransactionViewHolder>(null) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = TransactionItemBinding.inflate(inflater, parent, false)
        return TransactionViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val binding: TransactionItemBinding = holder.binding!!
        val viewModel = items[position]
        holder.itemView.setOnClickListener { onItemClick?.invoke(viewModel, position) }
        binding.viewModel = viewModel
    }
}