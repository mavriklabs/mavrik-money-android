package com.syject.mavrik.ui.custom.recycler.notification.group

import android.view.LayoutInflater
import android.view.ViewGroup
import com.syject.mavrik.base.BaseRecyclerViewAdapter
import com.syject.mavrik.databinding.NotificationGroupItemBinding
import com.syject.mavrik.model.notification.NotificationGroup

class NotificationGroupRecyclerViewAdapter :
    BaseRecyclerViewAdapter<NotificationGroup, NotificationGroupViewHolder>(null) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationGroupViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = NotificationGroupItemBinding.inflate(inflater, parent, false)
        return NotificationGroupViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: NotificationGroupViewHolder, position: Int) {
        val binding: NotificationGroupItemBinding = holder.binding!!
        val viewModel = items[position]
        holder.itemView.setOnClickListener { onItemClick?.invoke(viewModel, position) }
        binding.viewModel = viewModel
    }
}