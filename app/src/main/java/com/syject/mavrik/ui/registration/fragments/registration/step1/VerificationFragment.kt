package com.syject.mavrik.ui.registration.fragments.registration.step1

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.text.toSpannable
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep1VerificationBinding
import kotlinx.android.synthetic.main.fragment_step_1_verification.*

class VerificationFragment
    : BaseFragment<FragmentStep1VerificationBinding, VerificationViewModel>() {

    private val linkText = arrayOf("Didn't receive it? ", "Resend")

    override fun initViews() {
        setSpannableText()
    }

    private fun setSpannableText() {
        val ssb = SpannableStringBuilder()
            .append(linkText[0])
            .append(linkText[1], getResendClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        resendButton.text = ssb.toSpannable()
        resendButton.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun getResendClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {
            (requireParentFragment() as Step1ParentFragment).resendVerificationCode()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_step_1_verification

    override fun provideViewModel(): VerificationViewModel = VerificationViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}