package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.DialogVerificationResendFragmentEditBinding
import com.syject.mavrik.ui.dialogs.verification.resend.ResendDialog
import com.syject.mavrik.utlis.Constants

class EditEmailFragment :
    BaseFragment<DialogVerificationResendFragmentEditBinding, EditEmailViewModel>() {

    override fun getLayoutId(): Int = R.layout.dialog_verification_resend_fragment_edit

    override fun provideViewModel(): EditEmailViewModel = EditEmailViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_MAIN_VERIFICATION_CANCEL_EDIT -> {
                (requireParentFragment() as ResendDialog).setResendFragment()
            }
            Constants.CALL_CODE_MAIN_RESEND_EMAIL_VERIFICATION -> {
                (requireParentFragment() as ResendDialog).setResentFragment()
            }
        }
    }
}