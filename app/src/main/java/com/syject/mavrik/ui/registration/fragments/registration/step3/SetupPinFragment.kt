package com.syject.mavrik.ui.registration.fragments.registration.step3

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep3SetupPinBinding
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants

class SetupPinFragment : BaseFragment<FragmentStep3SetupPinBinding, SetupPinViewModel> () {
    override fun getLayoutId(): Int = R.layout.fragment_step_3_setup_pin

    override fun provideViewModel(): SetupPinViewModel = SetupPinViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SETUP_PIN_DONE -> {
                (requireActivity() as RegistrationActivity).onRegistrationCompleted()
            }
        }
    }
}