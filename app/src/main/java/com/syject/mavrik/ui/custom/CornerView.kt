package com.syject.mavrik.ui.custom

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.syject.mavrik.R
import com.syject.mavrik.utlis.asColor

class CornerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    @SuppressLint("DrawAllocation")
    override fun onDraw(canvas: Canvas) {
        var paint = Paint().apply {
            color = R.color.white.asColor()
            style = Paint.Style.FILL
        }
        var rect = Rect(0, 0, width, height)
        canvas.drawRect(rect, paint)

        paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            color = R.color.white.asColor()
            style = Paint.Style.FILL
            xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }
        canvas.drawRoundRect(
            0f,
            0f,
            width.toFloat(),
            height.toFloat(),
            15f * resources.displayMetrics.density + 0.5f,
            15f * resources.displayMetrics.density + 0.5f,
            paint
        )

        paint = Paint().apply {
            color = R.color.white.asColor()
            style = Paint.Style.FILL
            xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
        }
        rect = Rect(
            0,
            (15f * resources.displayMetrics.density + 0.5f).toInt(),
            width,
            height
        )
        canvas.drawRect(rect, paint)
    }

}