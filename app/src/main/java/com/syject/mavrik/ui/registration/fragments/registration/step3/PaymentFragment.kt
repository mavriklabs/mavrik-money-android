package com.syject.mavrik.ui.registration.fragments.registration.step3

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep3PaymentBinding
import com.syject.mavrik.utlis.Constants

class PaymentFragment : BaseFragment<FragmentStep3PaymentBinding, PaymentViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_step_3_payment

    override fun provideViewModel(): PaymentViewModel = PaymentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_REGISTER_NEXT_BUTTON_CLICK -> {
                (requireParentFragment() as Step3ParentFragment).setSelectFragment()
            }
        }
    }
}