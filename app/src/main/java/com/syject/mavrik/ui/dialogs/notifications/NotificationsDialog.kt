package com.syject.mavrik.ui.dialogs.notifications

import android.view.Gravity
import android.view.ViewGroup
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogNotificationsBinding

class NotificationsDialog : BaseDialog<DialogNotificationsBinding, NotificationsViewModel>() {

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setGravity(Gravity.BOTTOM)
            it.setBackgroundDrawableResource(android.R.color.transparent)
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_notifications

    override fun provideViewModel(): NotificationsViewModel = NotificationsViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}