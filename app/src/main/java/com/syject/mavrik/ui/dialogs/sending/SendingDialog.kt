package com.syject.mavrik.ui.dialogs.sending

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogSendingBinding

class SendingDialog : BaseDialog<DialogSendingBinding, SendingViewModel>() {
    override fun getLayoutId(): Int = R.layout.dialog_sending

    override fun provideViewModel(): SendingViewModel = SendingViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}