package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import androidx.fragment.app.DialogFragment
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.DialogVerificationResendFragmentResentBinding
import com.syject.mavrik.utlis.Constants

class EmailResentFragment :
    BaseFragment<DialogVerificationResendFragmentResentBinding, EmailResentViewModel>() {
    override fun getLayoutId(): Int = R.layout.dialog_verification_resend_fragment_resent

    override fun provideViewModel(): EmailResentViewModel = EmailResentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_DISMISS_DIALOG -> {
                (requireParentFragment() as DialogFragment).dismiss()
            }
        }
    }
}