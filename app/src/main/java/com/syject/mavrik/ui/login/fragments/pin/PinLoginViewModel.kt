package com.syject.mavrik.ui.login.fragments.pin

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class PinLoginViewModel(view: CallableView) : BaseViewModel(view) {

    var pin = ""

    fun onSignUpButtonClick(v: View) {
        callView(Constants.CALL_CODE_SIGN_UP)
    }

    fun onDoneButtonClick() {
        callView(Constants.CALL_CODE_LOGIN_COMPLETED)
    }

}