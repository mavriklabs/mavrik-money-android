package com.syject.mavrik.ui.dialogs.transactions

import android.view.Gravity
import android.view.ViewGroup
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogTransactionsBinding
import com.syject.mavrik.utlis.Constants

class TransactionsDialog : BaseDialog<DialogTransactionsBinding, TransactionsViewModel>() {

    override fun onStart() {
        super.onStart()
        dialog?.window?.let {
            it.setGravity(Gravity.BOTTOM)
            it.setBackgroundDrawableResource(android.R.color.transparent)
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_transactions

    override fun provideViewModel(): TransactionsViewModel = TransactionsViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_DISMISS_DIALOG -> {
                dismiss()
            }
        }
    }
}