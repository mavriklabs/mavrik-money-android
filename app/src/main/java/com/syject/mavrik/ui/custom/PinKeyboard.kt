package com.syject.mavrik.ui.custom

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputConnection
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import com.syject.mavrik.R
import kotlinx.android.synthetic.main.pin_keyboard.view.*


class PinKeyboard @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), View.OnClickListener {

    lateinit var inputConnection: InputConnection
    lateinit var onDoneButtonClick: () -> Unit

    init {
        View.inflate(context, R.layout.pin_keyboard, this)
        button0.setOnClickListener(this)
        button1.setOnClickListener(this)
        button2.setOnClickListener(this)
        button3.setOnClickListener(this)
        button4.setOnClickListener(this)
        button5.setOnClickListener(this)
        button6.setOnClickListener(this)
        button7.setOnClickListener(this)
        button8.setOnClickListener(this)
        button9.setOnClickListener(this)
        buttonDot.setOnClickListener(this)
        buttonBackspace.setOnClickListener(this)
        buttonDone.setOnClickListener(this)
    }

    override fun onClick(button: View) {
        when {
            button.id == R.id.buttonBackspace -> {
                val selectedText = inputConnection.getSelectedText(0)

                if (selectedText.isNullOrEmpty()) {
                    inputConnection.deleteSurroundingText(1, 0)
                } else {
                    inputConnection.commitText("", 1)
                }
            }
            button.id == R.id.buttonDone -> {
                onDoneButtonClick()
                visibility = View.GONE
            }
            button.id != R.id.buttonDot -> {
                inputConnection.commitText((button as AppCompatButton).text, 1)
            }
        }
    }
}