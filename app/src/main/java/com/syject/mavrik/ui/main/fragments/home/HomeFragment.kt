package com.syject.mavrik.ui.main.fragments.home

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.text.toSpannable
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentMainHomeBinding
import com.syject.mavrik.ui.dialogs.notifications.NotificationsDialog
import com.syject.mavrik.ui.dialogs.verification.resend.ResendDialog
import com.syject.mavrik.ui.dialogs.verification.verified.VerifiedDialog
import com.syject.mavrik.ui.main.MainActivity
import com.syject.mavrik.utlis.Constants
import com.syject.mavrik.utlis.asColor
import kotlinx.android.synthetic.main.fragment_main_home.*

class HomeFragment(
    private val isUserVerified: Boolean = true
) : BaseFragment<FragmentMainHomeBinding, HomeViewModel>() {

    private val emailVerificationInstructionText = arrayOf(
        "Important!",
        " Please make sure you’ve verified your email by clicking on the link we’ve sent to the email you provided."
    )

    override fun initViews() {
        initMulticoloredText()
    }

    private fun initMulticoloredText() {
        emailVerificationInstruction.text = SpannableStringBuilder()
            .append(
                emailVerificationInstructionText[0],
                ForegroundColorSpan(R.color.alert_red.asColor()),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            .append(emailVerificationInstructionText[1])
            .toSpannable()
    }

    override fun getLayoutId(): Int = R.layout.fragment_main_home

    override fun provideViewModel(): HomeViewModel = HomeViewModel(
        if (isUserVerified) View.GONE else View.VISIBLE,
        this
    )

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SHOW_ACCOUNT_BALANCE -> {
                (requireActivity() as MainActivity).setAccountBalanceFragment()
            }
            Constants.CALL_CODE_MAIN_SHOW_NOTIFICATIONS -> {
                NotificationsDialog().show(requireFragmentManager(), null)
            }
            Constants.CALL_CODE_MAIN_SHOW_RESEND_VERIFICATION_DIALOG -> {
                ResendDialog().show(requireFragmentManager(), null)
            }
            Constants.CALL_CODE_MAIN_SHOW_VERIFIED_DIALOG -> {
                VerifiedDialog().show(requireFragmentManager(), null)
            }
        }
    }
}