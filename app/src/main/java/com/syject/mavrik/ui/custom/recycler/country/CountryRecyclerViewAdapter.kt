package com.syject.mavrik.ui.custom.recycler.country

import android.view.LayoutInflater
import android.view.ViewGroup
import com.syject.mavrik.base.BaseRecyclerViewAdapter
import com.syject.mavrik.databinding.CountryItemBinding
import com.syject.mavrik.model.enums.country.Country

class CountryRecyclerViewAdapter(
    onItemClick: (item: Country, position: Int) -> Unit
) : BaseRecyclerViewAdapter<Country, CountryViewHolder>(onItemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CountryItemBinding.inflate(inflater, parent, false)
        return CountryViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        val binding: CountryItemBinding = holder.binding!!
        val viewModel = items[position]
        holder.itemView.setOnClickListener { onItemClick?.invoke(viewModel, position) }
        binding.viewModel = viewModel
    }
}