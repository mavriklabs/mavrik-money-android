package com.syject.mavrik.ui.make.photo

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class MakePhotoViewModel(view: CallableView) : BaseViewModel(view) {

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAKE_PHOTOS_BACK)
    }

}