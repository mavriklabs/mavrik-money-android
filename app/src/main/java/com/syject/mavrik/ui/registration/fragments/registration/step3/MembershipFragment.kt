package com.syject.mavrik.ui.registration.fragments.registration.step3

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep3MembershipBinding
import com.syject.mavrik.utlis.Constants

class MembershipFragment : BaseFragment<FragmentStep3MembershipBinding, MembershipViewModel>() {
    override fun getLayoutId(): Int = R.layout.fragment_step_3_membership

    override fun provideViewModel(): MembershipViewModel = MembershipViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_PREMIUM_MEMBERSHIP -> {
                (requireParentFragment() as Step3ParentFragment).setPaymentFragment()
            }
            Constants.CALL_CODE_BASIC_MEMBERSHIP -> {
                (requireParentFragment() as Step3ParentFragment).setSelectFragment()
            }
        }
    }
}