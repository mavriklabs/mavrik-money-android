package com.syject.mavrik.ui.dialogs.verification.resend

import android.view.Gravity
import android.view.ViewGroup
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogVerificationResendBinding
import com.syject.mavrik.ui.dialogs.verification.resend.frgments.EditEmailFragment
import com.syject.mavrik.ui.dialogs.verification.resend.frgments.EmailResentFragment
import com.syject.mavrik.ui.dialogs.verification.resend.frgments.ResendEmailFragment

class ResendDialog : BaseDialog<DialogVerificationResendBinding, ResendViewModel>() {

    override fun initViews() {
        dialog?.window?.let {
            it.setGravity(Gravity.CENTER)
            it.setBackgroundDrawableResource(android.R.color.transparent)
            it.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
        setResendFragment()
    }

    fun setResendFragment() {
        childFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, ResendEmailFragment())
            .commit()
    }

    fun setEditFragment() {
        childFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, EditEmailFragment())
            .commit()
    }

    fun setResentFragment() {
        childFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, EmailResentFragment())
            .commit()
    }

    override fun getLayoutId(): Int = R.layout.dialog_verification_resend

    override fun provideViewModel(): ResendViewModel = ResendViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}