package com.syject.mavrik.ui.registration.fragments.registration.step1

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep1ParentBinding
import com.syject.mavrik.ui.custom.ViewPagerAdapter
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_step_1_parent.*

class Step1ParentFragment : BaseFragment<FragmentStep1ParentBinding, Step1ParentViewModel>() {

    private val accountDetailsFragment: AccountDetailsFragment = AccountDetailsFragment()
    private val verificationFragment: VerificationFragment = VerificationFragment()
    private val basicInfoFragment: BasicInfoFragment = BasicInfoFragment()

    override fun initViews() {
        accountDetailsFragment
        pager.adapter = ViewPagerAdapter(
           childFragmentManager,
            listOf(
                accountDetailsFragment,
                verificationFragment,
                basicInfoFragment
            )
        )
        dotsIndicator.setupWithViewPager(pager)
    }

    fun resendVerificationCode() {
        pager.currentItem = 0
    }

    override fun getLayoutId(): Int = R.layout.fragment_step_1_parent

    override fun provideViewModel(): Step1ParentViewModel = Step1ParentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_REGISTER_NEXT_BUTTON_CLICK -> {
                if (pager.currentItem < 2) {
                    pager.currentItem = pager.currentItem + 1
                    if (pager.currentItem == 1) {
                        val accountDetailsViewModel = accountDetailsFragment.viewModel
                        verificationFragment.viewModel.phone =
                            accountDetailsViewModel.country.phoneCode +
                            accountDetailsViewModel.phone
                    }
                } else {
                    (requireActivity() as RegistrationActivity).setStep2Fragment()
                }
            }
            Constants.CALL_CODE_REGISTER_BACK -> {
                if (pager.currentItem > 0) {
                    pager.currentItem = pager.currentItem - 1
                } else {
                    (requireActivity() as RegistrationActivity).setExplainerFragment()
                }
            }
        }
    }
}