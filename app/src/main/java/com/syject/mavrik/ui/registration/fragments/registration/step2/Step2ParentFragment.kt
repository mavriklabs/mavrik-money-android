package com.syject.mavrik.ui.registration.fragments.registration.step2

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep2ParentBinding
import com.syject.mavrik.ui.make.photo.MakePhotoActivity
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants

class Step2ParentFragment : BaseFragment<FragmentStep2ParentBinding, Step2ParentViewModel>() {

    override fun initViews() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, AddressProofFragment())
            .commit()
    }

    fun makePhotos() {
        if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) !=
            PackageManager.PERMISSION_GRANTED) {

            requestPermissions(arrayOf(Manifest.permission.CAMERA), Constants.REQUEST_CODE_CAMERA_PERMISSIONS)
        } else {
            val intent = Intent(requireContext(), MakePhotoActivity::class.java)
            startActivityForResult(intent, Constants.REQUEST_CODE_MAKE_PHOTOS)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Constants.REQUEST_CODE_CAMERA_PERMISSIONS -> {
                if (!grantResults.contains(PackageManager.PERMISSION_DENIED)) {
                    val intent = Intent(requireContext(), MakePhotoActivity::class.java)
                    startActivityForResult(intent, Constants.REQUEST_CODE_MAKE_PHOTOS)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.REQUEST_CODE_MAKE_PHOTOS -> {
                    (requireActivity() as RegistrationActivity).setStep3Fragment()
                }
            }
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_step_2_parent

    override fun provideViewModel(): Step2ParentViewModel = Step2ParentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_REGISTER_BACK -> {
                (requireActivity() as RegistrationActivity).setStep1Fragment()
            }
        }
    }
}