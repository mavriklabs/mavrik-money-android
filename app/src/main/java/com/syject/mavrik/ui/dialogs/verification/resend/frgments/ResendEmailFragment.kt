package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.text.toSpannable
import androidx.fragment.app.DialogFragment
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.DialogVerificationResendFragmentResendBinding
import com.syject.mavrik.ui.dialogs.verification.resend.ResendDialog
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.dialog_verification_resend_fragment_resend.*

class ResendEmailFragment :
    BaseFragment<DialogVerificationResendFragmentResendBinding, ResendEmailViewModel>() {

    private val linkText = arrayOf(
        "The verification email will be resent to\njanedoe123@gmail.com. If you’d like to\nmake any changes you can ",
        "edit email",
        "."
    )

    override fun initViews() {
        setSpannableText()
    }

    private fun setSpannableText() {
        instruction.text = SpannableStringBuilder()
            .append(linkText[0])
            .append(linkText[1], getEditEmailClickableSpan(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            .append(linkText[2])
            .toSpannable()
        instruction.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun getEditEmailClickableSpan() = object : ClickableSpan() {
        override fun onClick(p0: View) {
            (requireParentFragment() as ResendDialog).setEditFragment()
        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_verification_resend_fragment_resend

    override fun provideViewModel(): ResendEmailViewModel = ResendEmailViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_DISMISS_DIALOG -> {
                (requireParentFragment() as DialogFragment).dismiss()
            }
            Constants.CALL_CODE_MAIN_RESEND_EMAIL_VERIFICATION -> {
                (requireParentFragment() as ResendDialog).setResentFragment()
            }
        }
    }
}