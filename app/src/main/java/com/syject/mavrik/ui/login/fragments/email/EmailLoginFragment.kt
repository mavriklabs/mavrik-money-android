package com.syject.mavrik.ui.login.fragments.email

import android.content.pm.PackageManager
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import androidx.core.text.toSpannable
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentLoginEmailBinding
import com.syject.mavrik.ui.forgot.password.ForgotPasswordActivity
import com.syject.mavrik.ui.login.LoginActivity
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_login_email.*

class EmailLoginFragment : BaseFragment<FragmentLoginEmailBinding, EmailLoginViewModel>() {

    private val linkText = arrayOf("Forgot Password?", " Or login with ", "PIN", " or ", "Touch ID", ".")

    override fun initViews() {
        setSpannableText()
    }

    private fun setSpannableText() {
        var ssb = SpannableStringBuilder()
            .append(linkText[0], getForgotClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            .append(linkText[1])
            .append(linkText[2], getPinClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        if (requireActivity().packageManager.hasSystemFeature(PackageManager.FEATURE_FINGERPRINT)) {
            ssb = ssb
                .append(linkText[3])
                .append(linkText[4], getTouchClickableSpan(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        }
        ssb = ssb
            .append(linkText[5])
        navigationLinks.text = ssb.toSpannable()
        navigationLinks.movementMethod = LinkMovementMethod.getInstance()
    }

    private fun getForgotClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {
            navigateTo(ForgotPasswordActivity::class.java)
        }
    }

    private fun getPinClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {
            (requireActivity() as LoginActivity).setPinFragment()
        }
    }

    private fun getTouchClickableSpan(): ClickableSpan = object : ClickableSpan() {
        override fun onClick(p0: View) {

        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_login_email

    override fun provideViewModel(): EmailLoginViewModel = EmailLoginViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SIGN_UP -> {
                navigateTo(RegistrationActivity::class.java)
            }
            Constants.CALL_CODE_LOGIN_COMPLETED -> {
                (requireActivity() as LoginActivity).onLoginCompleted()
            }
        }
    }
}