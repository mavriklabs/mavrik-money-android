package com.syject.mavrik.ui.main

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.ActivityMainBinding
import com.syject.mavrik.model.account.Account
import com.syject.mavrik.ui.dialogs.transactions.TransactionsDialog
import com.syject.mavrik.ui.main.fragments.account.balance.AccountBalanceFragment
import com.syject.mavrik.ui.main.fragments.balance.BalanceFragment
import com.syject.mavrik.ui.main.fragments.home.HomeFragment
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private var homeFragment: HomeFragment? = null
    private var accountFragment: AccountBalanceFragment? = null
    private var balanceFragment: BalanceFragment? = null
    private var currentFragment: BaseFragment<*,*>? = null

    override fun initViews() {
        setHomeFragment()
    }

    private fun setHomeFragment() {
        val isUserVerified = intent.getBooleanExtra(
            Constants.EXTRA_KEY_IS_USER_VERIFIED,
            true
        )
        intent.putExtra(Constants.EXTRA_KEY_IS_USER_VERIFIED, true)
        homeFragment = HomeFragment(isUserVerified)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, homeFragment!!)
            .commit()
        currentFragment = homeFragment

        viewModel.homeScreenSelected = true
        viewModel.accountScreenSelected = false
    }

    fun setAccountBalanceFragment() {
        accountFragment = AccountBalanceFragment(homeFragment!!.viewModel.account!!)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentContainer, accountFragment!!)
            .commit()
        currentFragment = accountFragment

        viewModel.homeScreenSelected = false
        viewModel.accountScreenSelected = true
    }

    fun openMenu() {
        root.openDrawer(drawer)
    }

    fun setBalanceFragment() {
        balanceFragment = BalanceFragment(accountFragment!!.account)
        supportFragmentManager
            .beginTransaction()
            .hide(accountFragment!!)
            .add(R.id.fragmentContainer, balanceFragment!!)
            .commit()
    }

    fun backFromBalanceFragment() {
        supportFragmentManager
            .beginTransaction()
            .hide(balanceFragment!!)
            .show(accountFragment!!)
            .commit()
        balanceFragment = null
    }

    override fun getLayoutId(): Int = R.layout.activity_main

    override fun provideViewModel(): MainViewModel = MainViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_MAIN_HOME -> {
                if (currentFragment !== homeFragment) {
                    setHomeFragment()
                    root.closeDrawer(drawer)
                }
            }
            Constants.CALL_CODE_MAIN_ACCOUNT -> {
                if (currentFragment !== accountFragment) {
                    accountFragment = AccountBalanceFragment(Account(
                        "Crypto Interest Account",
                        "$ 6,500.00",
                        "0xC8B6ec70d3...",
                        "6% Annually",
                        "$ 6,500.00",
                        "$ 123.00",
                        "$ 1,234.00"
                    ))
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, accountFragment!!)
                        .commit()
                    currentFragment = accountFragment

                    viewModel.homeScreenSelected = false
                    viewModel.accountScreenSelected = true

                    root.closeDrawer(drawer)
                }
            }
            Constants.CALL_CODE_MAIN_SHOW_TRANSACTIONS -> {
                TransactionsDialog().show(supportFragmentManager, null)
            }
        }
    }
}