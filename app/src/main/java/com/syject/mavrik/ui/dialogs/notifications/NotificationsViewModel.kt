package com.syject.mavrik.ui.dialogs.notifications

import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.model.notification.Notification
import com.syject.mavrik.model.notification.NotificationGroup
import com.syject.mavrik.ui.custom.recycler.notification.NotificationRecyclerViewAdapter
import com.syject.mavrik.ui.custom.recycler.notification.group.NotificationGroupRecyclerViewAdapter

class NotificationsViewModel(view: CallableView) : BaseViewModel(view) {

    val notificationGroupAdapter: NotificationGroupRecyclerViewAdapter =
        NotificationGroupRecyclerViewAdapter().apply {
            val recentNotifications = listOf(
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna."
                ),
                Notification(
                    "Lorem Ipsum Dolor",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                ),
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                )
            )
            val groupRecent =
                NotificationGroup("Recent", NotificationRecyclerViewAdapter().apply {
                    setData(recentNotifications)
                })

            val lastMonthNotifications = listOf(
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna."
                ),
                Notification(
                    "Lorem Ipsum Dolor",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                ),
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                )
            )
            val groupLastMonth =
                NotificationGroup("Last Month", NotificationRecyclerViewAdapter().apply {
                    setData(lastMonthNotifications)
                })

            val aprilNotifications = listOf(
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna."
                ),
                Notification(
                    "Lorem Ipsum Dolor",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                ),
                Notification(
                    "Lorem Ipsum",
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit."
                )
            )
            val groupApril =
                NotificationGroup("April", NotificationRecyclerViewAdapter().apply {
                    setData(aprilNotifications)
                })

            setData(listOf(groupRecent, groupLastMonth, groupApril))
            notifyDataSetChanged()
        }

}