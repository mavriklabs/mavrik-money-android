package com.syject.mavrik.ui.registration.fragments.registration.step1

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class Step1ParentViewModel(view: CallableView) : BaseViewModel(view) {

    fun onNextButtonClick(view: View) {
        callView(Constants.CALL_CODE_REGISTER_NEXT_BUTTON_CLICK)
    }

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_REGISTER_BACK)
    }

}