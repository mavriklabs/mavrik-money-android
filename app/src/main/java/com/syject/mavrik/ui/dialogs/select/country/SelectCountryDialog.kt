package com.syject.mavrik.ui.dialogs.select.country

import android.app.Activity
import android.content.Intent
import android.view.Gravity
import android.view.ViewGroup
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogSelectCountryBinding
import com.syject.mavrik.utlis.Constants

class SelectCountryDialog : BaseDialog<DialogSelectCountryBinding, SelectCountryViewModel>() {

    override fun initViews() {
        dialog?.window?.let {
            it.setGravity(Gravity.BOTTOM)
            it.setBackgroundDrawableResource(android.R.color.transparent)
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }

    override fun getLayoutId(): Int = R.layout.dialog_select_country

    override fun provideViewModel(): SelectCountryViewModel = SelectCountryViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SELECT_COUNTRY -> {
                val result = Intent()
                result.putExtra(Constants.EXTRA_KEY_COUNTRY, viewModel.country)
                targetFragment!!.onActivityResult(targetRequestCode, Activity.RESULT_OK, result)
                dismiss()
            }
            Constants.CALL_CODE_DISMISS_DIALOG -> {
                dismiss()
            }
        }
    }
}