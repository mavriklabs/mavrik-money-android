package com.syject.mavrik.ui.main.fragments.balance

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class BalanceViewModel(view: CallableView) : BaseViewModel(view) {

    fun onMenuButtonClick(view: View) {
        callView(Constants.CALL_CODE_OPEN_MENU)
    }

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_BALANCE_BACK)
    }

}