package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class EditEmailViewModel(view: CallableView) : BaseViewModel(view) {

    var email: String = ""

    fun onCancelButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_VERIFICATION_CANCEL_EDIT)
    }

    fun onConfirmButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_RESEND_EMAIL_VERIFICATION)
    }

}