package com.syject.mavrik.ui.registration.fragments.registration.step1

import android.app.Activity
import android.content.Intent
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep1AccountDetailsBinding
import com.syject.mavrik.model.enums.country.Country
import com.syject.mavrik.ui.dialogs.select.country.SelectCountryDialog
import com.syject.mavrik.utlis.Constants

class AccountDetailsFragment
    : BaseFragment<FragmentStep1AccountDetailsBinding, AccountDetailsViewModel>() {

    override fun getLayoutId(): Int = R.layout.fragment_step_1_account_details

    override fun provideViewModel(): AccountDetailsViewModel = AccountDetailsViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_SHOW_SELECT_COUNTRY_DIALOG -> {
                val dialog = SelectCountryDialog()
                dialog.setTargetFragment(
                    this,
                    Constants.REQUEST_CODE_SHOW_SELECT_COUNTRY_DIALOG
                )
                dialog.show(requireFragmentManager(), null)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                Constants.REQUEST_CODE_SHOW_SELECT_COUNTRY_DIALOG -> {
                    viewModel.country = data!!
                        .getSerializableExtra(Constants.EXTRA_KEY_COUNTRY) as Country
                }
            }
        }
    }
}