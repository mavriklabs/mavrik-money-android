package com.syject.mavrik.ui.main.fragments.home

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.model.account.Account
import com.syject.mavrik.ui.custom.recycler.account.AccountRecyclerViewAdapter
import com.syject.mavrik.utlis.Constants

class HomeViewModel(vov: Int, view: CallableView) : BaseViewModel(view) {

    var verificationOptionsVisibility = vov
    set(value) {
        field = value
        notifyChange()
    }

    val accountAdapter = AccountRecyclerViewAdapter(this::onAccountClick).apply {
        setData(listOf(
            Account(
                "Indian Savings Account",
                "\u20B9 5,40,000.00",
                "1234 - 5678 - 9123",
                "6% Annually",
                "$ 6,500.00",
                "$ 123.00",
                "$ 1,234.00"
            ),
            Account(
                "US Deposit Account",
                "$ 4,000.00",
                "1234 - 5678 - 9122",
                "6% Annually",
                "$ 6,500.00",
                "$ 123.00",
                "$ 1,234.00"
            ),
            Account(
                "Crypto Interest Account",
                "$ 6,500.00",
                "0xC8B6ec70d3...",
                "6% Annually",
                "$ 6,500.00",
                "$ 123.00",
                "$ 1,234.00"
            )
        ))
    }

    var account: Account? = null

    private fun onAccountClick(item: Account, position: Int) {
        account = item
        callView(Constants.CALL_CODE_SHOW_ACCOUNT_BALANCE)
    }

    fun onResendButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_SHOW_RESEND_VERIFICATION_DIALOG)
    }

    fun onVerifiedButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_SHOW_VERIFIED_DIALOG)
        verificationOptionsVisibility = View.GONE
    }

    fun onNotificationsButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_SHOW_NOTIFICATIONS)
    }

}