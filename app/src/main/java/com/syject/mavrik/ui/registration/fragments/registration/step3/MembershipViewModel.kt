package com.syject.mavrik.ui.registration.fragments.registration.step3

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class MembershipViewModel(view: CallableView) : BaseViewModel(view) {

    fun onSelectPremiumButtonClick(view: View) {
        callView(Constants.CALL_CODE_PREMIUM_MEMBERSHIP)
    }

    fun onSelectBasicButtonClick(view: View) {
        callView(Constants.CALL_CODE_BASIC_MEMBERSHIP)
    }

}