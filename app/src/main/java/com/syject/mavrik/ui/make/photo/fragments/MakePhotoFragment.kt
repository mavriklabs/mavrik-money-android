package com.syject.mavrik.ui.make.photo.fragments

import com.google.android.material.tabs.TabItem
import com.otaliastudios.cameraview.CameraListener
import com.otaliastudios.cameraview.PictureResult
import com.syject.mavrik.BR
import com.syject.mavrik.MavrikApplication
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentMakePhotoBinding
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_make_photo.*


class MakePhotoFragment(
    private val subtitle: String,
    private val instruction: String,
    private val position: Int,
    private val onMakePhotoButtonClick: () -> Unit
) : BaseFragment<FragmentMakePhotoBinding, MakePhotoFragmentViewModel>() {

    override fun initViews() {
        camera.setLifecycleOwner(this)
        camera.addCameraListener(object : CameraListener() {
            override fun onPictureTaken(result: PictureResult) {
                result.toBitmap { bitmap ->
                    MavrikApplication.instance.setImage(position, bitmap!!)
                    viewModel.picture = bitmap
                    onMakePhotoButtonClick()
                }
            }
        })
        binding.subtitle.text = subtitle
        binding.instruction.text = instruction
        dotsIndicator.apply {
            addView(TabItem(requireContext()))
            addView(TabItem(requireContext()))
            addView(TabItem(requireContext()))
            selectTab(getTabAt(position))
        }
        blocker.setOnTouchListener { _, _ -> true }
    }

    override fun getLayoutId(): Int = R.layout.fragment_make_photo

    override fun provideViewModel(): MakePhotoFragmentViewModel = MakePhotoFragmentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_MAKE_PHOTO_BUTTON_CLICK -> {
                camera.takePicture()
            }
        }
    }
}