package com.syject.mavrik.ui.registration.fragments.registration.step2

import androidx.lifecycle.Observer
import com.syject.mavrik.BR
import com.syject.mavrik.MavrikApplication
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep2QuickCheckBinding
import com.syject.mavrik.utlis.Constants
import kotlinx.android.synthetic.main.fragment_step_2_quick_check.*

class QuickCheckFragment(
    private val position: Int,
    private val onRedoButtonClick: () -> Unit,
    private val onNextButtonClick: () -> Unit
) : BaseFragment<FragmentStep2QuickCheckBinding, QuickCheckViewModel>() {

    override fun initViews() {
        super.initViews()
        MavrikApplication.instance.images.observe(viewLifecycleOwner, Observer {
            it.get(position)?.let { image ->
                picture.setImageBitmap(image)
            }
        })
    }

    override fun getLayoutId(): Int = R.layout.fragment_step_2_quick_check

    override fun provideViewModel(): QuickCheckViewModel = QuickCheckViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_QUICK_CHECK_REDO -> {
                onRedoButtonClick()
            }
            Constants.CALL_CODE_QUICK_CHECK_NEXT -> {
                onNextButtonClick()
            }
        }
    }
}