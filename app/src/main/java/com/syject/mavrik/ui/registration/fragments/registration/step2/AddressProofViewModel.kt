package com.syject.mavrik.ui.registration.fragments.registration.step2

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class AddressProofViewModel(view: CallableView) : BaseViewModel(view) {

    var firstOptionSelected = false
        set(value) {
            field = value
            notifyChange()
        }
    var secondOptionSelected = false
        set(value) {
            field = value
            notifyChange()
        }
    var thirdOptionSelected = false
        set(value) {
            field = value
            notifyChange()
        }

    fun firstOptionClick(view: View) {
        if (!firstOptionSelected) {
            firstOptionSelected = true
            secondOptionSelected = false
            thirdOptionSelected = false
        }
    }

    fun secondOptionClick(view: View) {
        if (!secondOptionSelected) {
            firstOptionSelected = false
            secondOptionSelected = true
            thirdOptionSelected = false
        }
    }

    fun thirdOptionClick(view: View) {
        if (!thirdOptionSelected) {
            firstOptionSelected = false
            secondOptionSelected = false
            thirdOptionSelected = true
        }
    }

    fun onNextButtonClick(view: View) {
        if (firstOptionSelected || secondOptionSelected || thirdOptionSelected) {
            callView(Constants.CALL_CODE_MAKE_PHOTOS)
        }
    }

}