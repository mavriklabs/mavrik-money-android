package com.syject.mavrik.ui.registration.fragments.credencials

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class CredentialsViewModel(view: CallableView) : BaseViewModel(view) {

    var email = ""

    var password = ""

    fun onBackButtonClick(view: View) {
        callView(Constants.CALL_CODE_CREDENTIALS_BACK)
    }

    fun onGoogleButtonClick(view: View) {

    }

    fun onNextButtonClick(view: View) {
        callView(Constants.CALL_CODE_CREDENTIALS_NEXT)
    }

}