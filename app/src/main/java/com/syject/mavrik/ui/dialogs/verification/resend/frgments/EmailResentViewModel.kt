package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class EmailResentViewModel(view: CallableView) : BaseViewModel(view) {

    fun onCustomerServiceButtonClick(view: View) {

    }

    fun onGotItButtonClick(view: View) {
        callView(Constants.CALL_CODE_DISMISS_DIALOG)
    }

}