package com.syject.mavrik.ui.registration.fragments.explainer

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentExplainerChildBinding
import com.syject.mavrik.ui.custom.BulletText
import kotlinx.android.synthetic.main.fragment_explainer_child.*

class ExplainerChildFragment(
    private val subtitleText: String,
    private val pointsText: Array<String>
) : BaseFragment<FragmentExplainerChildBinding, ExplainerChildViewModel>() {

    override fun initViews() {
        subtitle.text = subtitleText
        for (pointText in pointsText) {
            pointsContainer.addView(BulletText(requireContext()).apply { setText(pointText) })
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_explainer_child

    override fun provideViewModel(): ExplainerChildViewModel = ExplainerChildViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}