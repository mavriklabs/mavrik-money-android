package com.syject.mavrik.ui.custom.recycler.country

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.syject.mavrik.model.enums.country.Country

@BindingAdapter("android:src")
fun onBindFlagIconForCountry(iv: ImageView, c: Country) {
    iv.setImageResource(c.countryFlag)
}