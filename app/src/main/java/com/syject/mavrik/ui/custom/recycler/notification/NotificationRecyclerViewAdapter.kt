package com.syject.mavrik.ui.custom.recycler.notification

import android.view.LayoutInflater
import android.view.ViewGroup
import com.syject.mavrik.base.BaseRecyclerViewAdapter
import com.syject.mavrik.databinding.NotificationItemBinding
import com.syject.mavrik.model.notification.Notification

class NotificationRecyclerViewAdapter :
    BaseRecyclerViewAdapter<Notification, NotificationViewHolder>(null) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotificationViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = NotificationItemBinding.inflate(inflater, parent, false)
        return NotificationViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: NotificationViewHolder, position: Int) {
        val binding: NotificationItemBinding = holder.binding!!
        val viewModel = items[position]
        holder.itemView.setOnClickListener { onItemClick?.invoke(viewModel, position) }
        binding.viewModel = viewModel
    }
}