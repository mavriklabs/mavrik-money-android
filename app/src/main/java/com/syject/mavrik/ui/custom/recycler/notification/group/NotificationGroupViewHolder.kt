package com.syject.mavrik.ui.custom.recycler.notification.group

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.syject.mavrik.databinding.NotificationGroupItemBinding

class NotificationGroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val binding: NotificationGroupItemBinding? = DataBindingUtil.bind(itemView)

}