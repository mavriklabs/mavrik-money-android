package com.syject.mavrik.ui.main

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class MainViewModel(view: CallableView) : BaseViewModel(view) {

    var homeScreenSelected = true
        set(value) {
            field = value
            notifyChange()
        }
    var accountScreenSelected = false
        set(value) {
            field = value
            notifyChange()
        }

    fun onHomeButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_HOME)
    }

    fun onAccountButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_ACCOUNT)
    }

    fun onDepositWithdrawButtonClick(view: View) {

    }

    fun onTransactionsButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_SHOW_TRANSACTIONS)
    }

    fun onPaymentDetailsButtonClick(view: View) {

    }

    fun onLogoutButtonClick(view: View) {

    }

}