package com.syject.mavrik.ui.registration.fragments.registration.step3

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.FragmentStep3ParentBinding
import com.syject.mavrik.ui.registration.RegistrationActivity
import com.syject.mavrik.utlis.Constants

class Step3ParentFragment : BaseFragment<FragmentStep3ParentBinding, Step3ParentViewModel>() {

    private var membershipFragment: MembershipFragment? = null
    private var paymentFragment: PaymentFragment? = null
    private var selectFragment: SelectFragment? = null
    private var setupPinFragment: SetupPinFragment? = null
    private var currentFragment: BaseFragment<*,*>? = null

    override fun initViews() {
        setMembershipFragment()
    }

    fun setMembershipFragment() {
        hideCurrentFragment()
        if (membershipFragment === null) {
            membershipFragment = MembershipFragment()
            childFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, membershipFragment!!)
                .commit()
        } else {
            childFragmentManager
                .beginTransaction()
                .show(membershipFragment!!)
                .commit()
        }
        currentFragment = membershipFragment
    }

    fun setPaymentFragment() {
        hideCurrentFragment()
        if (paymentFragment === null) {
            paymentFragment = PaymentFragment()
            childFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, paymentFragment!!)
                .commit()
        } else {
            childFragmentManager
                .beginTransaction()
                .show(paymentFragment!!)
                .commit()
        }
        currentFragment = paymentFragment
    }

    fun setSelectFragment() {
        hideCurrentFragment()
        if (selectFragment === null) {
            selectFragment = SelectFragment()
            childFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, selectFragment!!)
                .commit()
        } else {
            childFragmentManager
                .beginTransaction()
                .show(selectFragment!!)
                .commit()
        }
        currentFragment = selectFragment
    }

    fun setSetupPinFragment() {
        hideCurrentFragment()
        if (setupPinFragment === null) {
            setupPinFragment = SetupPinFragment()
            childFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, setupPinFragment!!)
                .commit()
        } else {
            childFragmentManager
                .beginTransaction()
                .show(setupPinFragment!!)
                .commit()
        }
        currentFragment = setupPinFragment
    }

    private fun hideCurrentFragment() {
        if (currentFragment !== null) {
            childFragmentManager
                .beginTransaction()
                .hide(currentFragment!!)
                .commit()
        }
    }

    override fun getLayoutId(): Int = R.layout.fragment_step_3_parent

    override fun provideViewModel(): Step3ParentViewModel = Step3ParentViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
        when (code) {
            Constants.CALL_CODE_REGISTER_BACK -> {
                when (currentFragment) {
                    membershipFragment -> {
                        (requireActivity() as RegistrationActivity).setStep2Fragment()
                    }
                    paymentFragment -> {
                        setMembershipFragment()
                    }
                    selectFragment -> {
                        setMembershipFragment()
                    }
                    setupPinFragment -> {
                        setSelectFragment()
                    }
                }
            }
        }
    }
}