package com.syject.mavrik.ui.login.fragments.email

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class EmailLoginViewModel(view: CallableView) : BaseViewModel(view) {

    var email = ""
    var password = ""

    fun onLoginButtonClick(view: View) {
        callView(Constants.CALL_CODE_LOGIN_COMPLETED)
    }

    fun onSignUpButtonClick(view: View) {
        callView(Constants.CALL_CODE_SIGN_UP)
    }
}