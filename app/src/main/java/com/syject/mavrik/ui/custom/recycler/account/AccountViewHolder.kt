package com.syject.mavrik.ui.custom.recycler.account

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.syject.mavrik.databinding.AccountItemBinding

class AccountViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val binding: AccountItemBinding? = DataBindingUtil.bind(itemView)

}