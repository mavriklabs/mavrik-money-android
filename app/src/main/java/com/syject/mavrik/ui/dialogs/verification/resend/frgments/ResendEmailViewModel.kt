package com.syject.mavrik.ui.dialogs.verification.resend.frgments

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class ResendEmailViewModel(view: CallableView) : BaseViewModel(view) {

    fun onCancelButtonClick(view: View) {
        callView(Constants.CALL_CODE_DISMISS_DIALOG)
    }


    fun onConfirmButtonClick(view: View) {
        callView(Constants.CALL_CODE_MAIN_RESEND_EMAIL_VERIFICATION)
    }

}