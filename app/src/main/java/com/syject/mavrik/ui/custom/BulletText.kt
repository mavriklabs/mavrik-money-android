package com.syject.mavrik.ui.custom

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.syject.mavrik.R
import kotlinx.android.synthetic.main.bullet_text.view.*

class BulletText @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    init {
        View.inflate(context, R.layout.bullet_text, this)
    }

    fun setText(string: String) {
        text.text = string
    }
}