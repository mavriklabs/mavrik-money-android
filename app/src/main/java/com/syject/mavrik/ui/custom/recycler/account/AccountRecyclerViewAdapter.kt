package com.syject.mavrik.ui.custom.recycler.account

import android.view.LayoutInflater
import android.view.ViewGroup
import com.syject.mavrik.base.BaseRecyclerViewAdapter
import com.syject.mavrik.databinding.AccountItemBinding
import com.syject.mavrik.model.account.Account

class AccountRecyclerViewAdapter(onItemClick: (Account, Int) -> Unit)
    : BaseRecyclerViewAdapter<Account, AccountViewHolder>(onItemClick) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AccountViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AccountItemBinding.inflate(inflater, parent, false)
        return AccountViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: AccountViewHolder, position: Int) {
        val binding: AccountItemBinding = holder.binding!!
        val viewModel = items[position]
        holder.itemView.setOnClickListener { onItemClick?.invoke(viewModel, position) }
        binding.viewModel = viewModel
    }
}