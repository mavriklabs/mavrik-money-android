package com.syject.mavrik.ui.dialogs.verification.verified.fragments

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class SuccessfulViewModel(view: CallableView) : BaseViewModel(view) {

    fun onDoneButtonClick(view: View) {
        callView(Constants.CALL_CODE_DISMISS_DIALOG)
    }

}