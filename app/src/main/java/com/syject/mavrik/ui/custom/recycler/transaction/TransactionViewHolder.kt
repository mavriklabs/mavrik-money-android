package com.syject.mavrik.ui.custom.recycler.transaction

import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.syject.mavrik.databinding.TransactionItemBinding

class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    val binding: TransactionItemBinding? = DataBindingUtil.bind(itemView)

}