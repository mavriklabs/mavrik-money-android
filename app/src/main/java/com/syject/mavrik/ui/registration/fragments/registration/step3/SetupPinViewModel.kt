package com.syject.mavrik.ui.registration.fragments.registration.step3

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.utlis.Constants

class SetupPinViewModel(view: CallableView) : BaseViewModel(view) {

    var pin = ""
    var pinConfirm = ""

    fun onDoneButtonClick(view: View) {
        callView(Constants.CALL_CODE_SETUP_PIN_DONE)
    }

}