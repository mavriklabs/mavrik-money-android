package com.syject.mavrik.ui.dialogs.verification.verified

import android.view.Gravity
import android.view.ViewGroup
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseDialog
import com.syject.mavrik.databinding.DialogVerificationVerifiedBinding
import com.syject.mavrik.ui.dialogs.verification.verified.fragments.SuccessfulFragment

class VerifiedDialog : BaseDialog<DialogVerificationVerifiedBinding, VerifiedViewModel>() {

    override fun initViews() {
        dialog?.window?.let {
            it.setGravity(Gravity.CENTER)
            it.setBackgroundDrawableResource(android.R.color.transparent)
            it.setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
        setSuccessfulFragment()
    }

    private fun setSuccessfulFragment() {
        childFragmentManager
            .beginTransaction()
            .add(R.id.fragmentContainer, SuccessfulFragment())
            .commit()
    }

    override fun getLayoutId(): Int = R.layout.dialog_verification_verified

    override fun provideViewModel(): VerifiedViewModel = VerifiedViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}