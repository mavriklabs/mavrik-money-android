package com.syject.mavrik.ui.registration

import android.content.Intent
import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.base.BaseFragment
import com.syject.mavrik.databinding.ActivityRegistrationBinding
import com.syject.mavrik.ui.main.MainActivity
import com.syject.mavrik.ui.registration.fragments.credencials.CredentialsFragment
import com.syject.mavrik.ui.registration.fragments.explainer.ExplainerParentFragment
import com.syject.mavrik.ui.registration.fragments.registration.step1.Step1ParentFragment
import com.syject.mavrik.ui.registration.fragments.registration.step2.Step2ParentFragment
import com.syject.mavrik.ui.registration.fragments.registration.step3.Step3ParentFragment
import com.syject.mavrik.utlis.Constants

class RegistrationActivity : BaseActivity<ActivityRegistrationBinding, RegistrationViewModel>() {

    private var credentialsFragment: CredentialsFragment? = null
    private var explainerFragment: ExplainerParentFragment? = null
    private var step1Fragment: Step1ParentFragment? = null
    private var step2Fragment: Step2ParentFragment? = null
    private var step3Fragment: Step3ParentFragment? = null
    private var currentFragment: BaseFragment<*,*>? = null

    override fun initViews() {
        setCredentialFragment()
    }

    fun setCredentialFragment() {
        hideCurrentFragment()
        if (credentialsFragment === null) {
            credentialsFragment = CredentialsFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, credentialsFragment!!)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .show(credentialsFragment!!)
                .commit()
        }
        currentFragment = credentialsFragment
    }

    fun setExplainerFragment() {
        hideCurrentFragment()
        if (explainerFragment === null) {
            explainerFragment = ExplainerParentFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, explainerFragment!!)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .show(explainerFragment!!)
                .commit()
        }
        currentFragment = explainerFragment
    }

    fun setStep1Fragment() {
        hideCurrentFragment()
        if (step1Fragment === null) {
            step1Fragment = Step1ParentFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, step1Fragment!!)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .show(step1Fragment!!)
                .commit()
        }
        currentFragment = step1Fragment
    }

    fun setStep2Fragment() {
        hideCurrentFragment()
        if (step2Fragment === null) {
            step2Fragment = Step2ParentFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, step2Fragment!!)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .show(step2Fragment!!)
                .commit()
        }
        currentFragment = step2Fragment
    }

    fun setStep3Fragment() {
        hideCurrentFragment()
        if (step3Fragment === null) {
            step3Fragment = Step3ParentFragment()
            supportFragmentManager
                .beginTransaction()
                .add(R.id.fragmentContainer, step3Fragment!!)
                .commit()
        } else {
            supportFragmentManager
                .beginTransaction()
                .show(step3Fragment!!)
                .commit()
        }
        currentFragment = step3Fragment
    }

    private fun hideCurrentFragment() {
        if (currentFragment !== null) {
            supportFragmentManager
                .beginTransaction()
                .hide(currentFragment!!)
                .commit()
        }
    }

    fun onRegistrationCompleted() {
        val intent = Intent(this, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Constants.EXTRA_KEY_IS_USER_VERIFIED, false)
        startActivity(intent)
    }

    override fun getLayoutId(): Int = R.layout.activity_registration

    override fun provideViewModel(): RegistrationViewModel = RegistrationViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}