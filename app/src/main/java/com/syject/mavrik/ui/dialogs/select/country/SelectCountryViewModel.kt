package com.syject.mavrik.ui.dialogs.select.country

import android.view.View
import com.syject.mavrik.base.BaseViewModel
import com.syject.mavrik.base.CallableView
import com.syject.mavrik.model.enums.country.Country
import com.syject.mavrik.ui.custom.recycler.country.CountryRecyclerViewAdapter
import com.syject.mavrik.utlis.Constants

class SelectCountryViewModel(view: CallableView) : BaseViewModel(view) {

    var country: Country? = null

    var countryAdapter = CountryRecyclerViewAdapter(this::handleCountryClick).apply {
        setData(Country.values().asList())
    }

    private fun handleCountryClick(item: Country, position: Int) {
        country = item
        callView(Constants.CALL_CODE_SELECT_COUNTRY)
    }

    fun onCloseButtonClick(view: View) {
        callView(Constants.CALL_CODE_DISMISS_DIALOG)
    }
}