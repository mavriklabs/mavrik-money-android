package com.syject.mavrik.ui.forgot.password

import com.syject.mavrik.BR
import com.syject.mavrik.R
import com.syject.mavrik.base.BaseActivity
import com.syject.mavrik.databinding.ActivityForgotPasswordBinding

class ForgotPasswordActivity
    : BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>() {
    override fun getLayoutId(): Int = R.layout.activity_forgot_password

    override fun provideViewModel(): ForgotPasswordViewModel = ForgotPasswordViewModel(this)

    override fun getViewModelVariableId(): Int = BR.viewModel

    override fun call(code: Int) {
    }
}