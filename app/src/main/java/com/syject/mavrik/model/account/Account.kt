package com.syject.mavrik.model.account

class Account(
    var name: String,
    var accountBalance: String,
    var accountNumber: String,
    var interest: String,

    var amountDeposited: String,
    var amountInterest: String,
    var predictedInterest: String
) {
}