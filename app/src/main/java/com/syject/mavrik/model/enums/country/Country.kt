package com.syject.mavrik.model.enums.country

import com.syject.mavrik.R

enum class Country(
    val countryName: String,
    val countryFlag: Int,
    val phoneCode: String
) {
    USA("United States", R.drawable.flag_usa, "+1"),
    AFGHANISTAN("Afghanistan", R.drawable.flag_afganistan, "+93"),
    ALBANIA("Albania", R.drawable.flag_albania, "+355"),
    ALGERIA("Algeria", R.drawable.flag_algeria, "+213"),
    AMERICAN_SAMOA("American Samoa", R.drawable.flag_american_samoa, "+1"),
    ANDORRA("Andorra", R.drawable.flag_andorra, "+376"),
    ANGOLA("Angola", R.drawable.flag_angola, "+244"),
    ANGUILLA("Anguilla", R.drawable.flag_anguilla, "+1"),
    ANTIGUA_BARBUDA("Antigua and Barbuda", R.drawable.flag_antigua_barbuda, "+1"),
    ARGENTINA("Argentina", R.drawable.flag_argentina, "+54"),
    ARMENIA("Armenia", R.drawable.flag_armenia, "+347"),
    INDIA("India", R.drawable.flag_india, "+94")
}