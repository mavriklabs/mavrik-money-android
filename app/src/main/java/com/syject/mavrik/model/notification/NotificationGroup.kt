package com.syject.mavrik.model.notification

import com.syject.mavrik.ui.custom.recycler.notification.NotificationRecyclerViewAdapter

class NotificationGroup(
    var title: String,
    var notificationsAdapter: NotificationRecyclerViewAdapter
) {
}