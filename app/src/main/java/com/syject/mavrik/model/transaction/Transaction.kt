package com.syject.mavrik.model.transaction

class Transaction(
    var year: String,
    var description: String,
    var sum: String,
    var code: String,
    var dateTime: String
) {
}