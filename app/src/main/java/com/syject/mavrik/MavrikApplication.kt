package com.syject.mavrik

import android.app.Application
import android.content.Context
import android.graphics.Bitmap
import android.util.SparseArray
import androidx.core.util.set
import androidx.lifecycle.MutableLiveData
import androidx.multidex.MultiDex

class MavrikApplication : Application() {

    var images = MutableLiveData<SparseArray<Bitmap>>().apply {
        value = SparseArray()
    }

    companion object {
        lateinit var instance: MavrikApplication
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    fun setImage(pos: Int, bitmap: Bitmap) {
        val array = images.value!!
        array[pos] = bitmap
        images.postValue(array)
    }
}